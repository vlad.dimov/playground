package utils

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"playground/models"
)

func FetchToken() (string, error) {
	resp, err := http.PostForm("http://localhost:8090/realms/libre/protocol/openid-connect/token",
		url.Values{
			"username":      {"admin@libremfg.com"},
			"password":      {"admin"},
			"grant_type":    {"password"},
			"client_id":     {"libreBaas"},
			"client_secret": {"pk98t8jVtwF9P8erRHZpLklWtz1TzGTR"},
		})

	if err != nil {
		fmt.Println("failed to fetch token")
		return "", err
	}

	body, err := io.ReadAll(resp.Body)

	if err != nil {
		fmt.Println("failed to read the body", err)
		return "", err
	}

	// fmt.Println(string(body[:]))

	var token models.KeycloakResponse
	json.Unmarshal(body, &token)
	fmt.Println(token.Access_token)

	resp.Body.Close()

	return token.Access_token, nil
}

func WriteTokenToDesktop(token string) {
	err := os.WriteFile("/Users/vladdimov/Desktop/token.txt", []byte(token), 0644)
	if err != nil {
		fmt.Println("Failed to write to file:", err)
		return
	}
	fmt.Println("Wrote to the file 'token.txt'.")
}
