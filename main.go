package main

import (
	"playground/utils"
)

func main() {
	token, err := utils.FetchToken()

	if err != nil {
		return
	}

	utils.WriteTokenToDesktop(token)
}
