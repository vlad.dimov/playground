package models

type KeycloakResponse struct {
	Access_token string
	Token_type   string
	Scope        string
}
